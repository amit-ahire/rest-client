/**
 * Created by amitahire on 8/8/16.
 */
$(document).ready(function(){
    var xhttp = new XMLHttpRequest();

    $("#submit").click(function(){
        var uri = $("#uri").val();
        var params = $("#params").val();
        var method = $("#method").val();

        xhttp.open(method,encodeURI(uri),true);
        xhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xhttp.setRequestHeader("Content-Length",params.length);
        xhttp.setRequestHeader("Connection","close");
        xhttp.onload = function(){
            $("#result").val(xhttp.responseText);
            //alert(xhttp.responseText);
        };
        //alert(params);
        xhttp.send(params);

    });
});