# README #

### Description ###

* This repository is basically a chrome extension to make HTTP request like GET, POST, PUT and DELETE to a particular URL.
* After sending request to URL, It will return some data that is also shown here
* Here you can also add parameters.
* Version : 1.0.0

### Setting Up environment ###

###** 1. Setup extension** ###
* Open chrome's settings
* Click on extension
* Enable developer mode by click on checkbox
* Click on load unpacked extension
* Select directory that you have cloned

###** 2. Dependencies for creating server** ###
** 2.1. Node js ** 

* sudo apt-get update
* sudo apt-get install build-essential
* sudo apt-get install nodejs

** 2.2. Npm **

* sudo apt-get install npm

** 2.3. Install modules require to create server **

* Go into server directory
* type : npm install
* If any error occurs try it with sudo

###** 3. Deployment ** ###
** 3.1 Server **

* First go into server folder
* You'll find server.js
* Run it using node server.js
* You can modify host and port number in server.js as per your choice

** 3.2 Extension **

* After loading extension you'll get an extension icon in browser's bar.
* Click on that icon a page will open.
* In that you can make HTTP request to particular URL

### References ###
In this application I've used below open source libraries.

** [1. Jquery](https://jquery.com/) **

Jquery is a fast and small javascript library which makes document traversal, event handling, etc much simpler with easy to use syntax.

** [2. Node js](https://nodejs.org/en/) **

Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world.

** [3. Hapi js](http://hapijs.com/) **

A rich framework for building applications and services
Hapi enables developers to focus on writing reusable application logic instead of spending time building infrastructure.

** [4. Joi js](https://github.com/hapijs/joi/blob/v9.0.4/API.md) **

It is oject schema description language and validator for JavaScript objects.
It makes validation easier