/**
 * Created by amitahire on 9/8/16.
 */
const Hapi = require("hapi");
const Joi = require("joi");
const _ = require("lodash");
const Server = new Hapi.Server();

var data =[
    {
        id:1,
        name: "XYZ",
        gender: "male"
    },
    {
        id:3,
        name: "ABC",
        gender: "female"
    },
    {
        id:7,
        name: "XQQ",
        gender: "male"
    }
];
function updateObject(data,up){
    i = _.findIndex(data,function(obj){
        return obj.id == up.id;
    });
    data[i].name = up.name ? up.name : data[i].name;
    data[i].gender = up.gender ? up.gender : data[i].gender;
    return data;
}
function deleteObject(data,id){
    return _.reject(data,function(obj){
        return obj.id == id;
    })
};
var getConfig = {
    handler: function(request,reply){
        reply(data);
    }
};
var postConfig = {
    handler: function(request,reply){
        data.push(request.payload);
        reply(data);
    },
    validate: {
        payload: {
            id: Joi.number().min(1).max(20).required(),
            name: Joi.string().required(),
            gender: Joi.any().valid(["male","female"])
        }
    }
};

var putConfig = {
    handler: function(request,reply){
        data = updateObject(data,request.payload);
        reply(data);
    },
    validate: {
        payload: {
            id: Joi.number().min(1).max(20).required(),
            name: Joi.string().optional(),
            gender: Joi.any().valid(["male","female"]).optional()
        }
    }
}

var deleteConfig = {
    handler: function(request,reply){
        data = deleteObject(data,request.payload.id);
        reply(data);
    },
    validate: {
        payload: {
            id: Joi.number().min(1).max(20).required()
        }
    }
};

Server.connection({
    host:"localhost",
    port:"8081"
});

Server.route([
    {
        method: "GET",
        path: "/",
        config: getConfig
    },
    {
        method: "POST",
        path: "/",
        config: postConfig

    },
    {
        method: "PUT",
        path: "/",
        config: putConfig
    },
    {
        method: "DELETE",
        path: "/",
        config: deleteConfig
    }
]);

Server.start( function(err) {
    if(err) throw err;
    console.log("Server is started at "+ Server.info.uri);
});